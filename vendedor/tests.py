from django.test import TestCase
from vendedor.models import Proveedor
from vendedor.forms import FormProveedor

class proveedoresTest(TestCase):
    def test_verPaginaListaProveedores(self):
        response = self.client.get('/vendedor/proveedores')
        self.assertEquals(response.status_code, 200)
    
    def test_verificarTemplateProveedores(self):
        response = self.client.get('/vendedor/proveedores')
        self.assertTemplateUsed(response, 'proveedores.html')
    
    def test_verPaginaAgregarProveedor(self):
        response = self.client.get('/vendedor/proveedores/nuevo')
        self.assertEquals(response.status_code, 200)
    
    def test_verificarTemplateAgregarProveedor(self):
        response = self.client.get('/vendedor/proveedores/nuevo')
        self.assertTemplateUsed(response, 'nuevo_proveedor.html')
    
    def test_verPaginaModificarProveedor(self):
        proveedor = Proveedor.objects.create(
            nombre = 'Colette',
            primerapeido = 'Coco',
            compania = 'The Coca-Cola Company',
            telefono = '4925553312'
        )
        response = self.client.get('/vendedor/proveedores/editar/'+str(proveedor.id)+'')
        self.assertEquals(response.status_code, 200)
    
    def test_verificarTemplateModificarProveedor(self):
        proveedor = Proveedor.objects.create(
            nombre = 'Colette',
            primerapeido = 'Coco',
            compania = 'The Coca-Cola Company',
            telefono = '4925553312'
        )
        response = self.client.get('/vendedor/proveedores/editar/'+str(proveedor.id)+'')
        self.assertTemplateUsed(response, 'nuevo_proveedor.html')
    
    def test_guardar_proveedor_form(self):
        proveedor = Proveedor.objects.create(
            nombre = 'Colette',
            primerapeido = 'Coco',
            compania = 'The Coca-Cola Company',
            telefono = '4925553312'
        )
        form = FormProveedor(
            data={
                'nombre': proveedor.nombre,
                'primerapeido': proveedor.primerapeido,
                'compania': proveedor.compania,
                'telefono': proveedor.telefono
            }
        )
        if form.is_valid():
            new_proveedor = form.save()
            self.assertEqual(new_proveedor, Proveedor.objects.latest('id'))