from django.db import models
from django.contrib.auth.models import User

class Vendedor(User):
    telefono = models.CharField('Numero Telefonico', max_length=15)

class Proveedor(models.Model):
    nombre = models.CharField('Nombre',max_length=20)
    primerapeido = models.CharField('Apellido', max_length=20)
    compania = models.CharField('compania',max_length=20)
    telefono = models.CharField('Numero Telefonico', max_length=15)    