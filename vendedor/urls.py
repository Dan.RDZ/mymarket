from django.urls import path
from vendedor.views import lista_proveedores, nuevo_proveedor, editar_proveedor, eliminar_proveedor

urlpatterns = [
    path('proveedores', lista_proveedores, name='lista_proveedores'),
    path('proveedores/nuevo', nuevo_proveedor, name='nuevo_proveedor'),
    path('proveedores/editar/<int:id>', editar_proveedor, name='editar_proveedor'),
    path('proveedores/eliminar/<int:id>', eliminar_proveedor, name='eliminar_proveedor'),
]