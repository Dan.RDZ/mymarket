from django import forms
from vendedor.models import Proveedor

class FormProveedor(forms.ModelForm):

    class Meta:
        model = Proveedor
        fields = ('nombre','primerapeido','compania','telefono')