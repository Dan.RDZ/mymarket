from django.shortcuts import render, redirect
from vendedor.models import Proveedor
from vendedor.forms import FormProveedor
from datetime import datetime, timedelta , date

def lista_proveedores(context):
    proveedores = Proveedor.objects.all()
    return render(context, 'proveedores.html',{'proveedores':proveedores})

def nuevo_proveedor(context):
    if (context.method == 'POST'):
       form = FormProveedor(context.POST)
       if form.is_valid():
           proveedor = form.save()
           proveedor.save()

           return redirect('/vendedor/proveedores')
    else:
        form = FormProveedor()
    return render(context, 'nuevo_proveedor.html',{'formulario':form,'accion':'Nuevo'})

def editar_proveedor(request, id):
    proveedor = Proveedor.objects.get(pk=id)
    if request.method == "POST":
        form = FormProveedor(request.POST, instance=proveedor)
        if form.is_valid():
            proveedor = form.save()
            proveedor.save()

            return redirect('/vendedor/proveedores')
    else:
        form = FormProveedor(instance=proveedor)
    return render(request, 'nuevo_proveedor.html', {'formulario': form,'accion':'Editar'})

def eliminar_proveedor(context, id):
    proveedor = Proveedor.objects.get(pk=id)
    proveedor.delete()
    return redirect('/vendedor/proveedores')