from django.shortcuts import render , redirect
from productos.models import Producto
from .forms import FormProducto
# Create your views here.
"""def mostrar_inicio(context):
    return render(context,'index.html')
"""
def lista_productos(context):
    productos = Producto.objects.all()
    return render(context, 'productos.html',{'productos':productos})

def nuevo_producto(context):
    if (context.method == 'POST'):
       form = FormProducto(context.POST)
       if form.is_valid():
           producto = form.save()
           producto.save()

           return redirect('/productos')

    else:
        form = FormProducto()
    return render(context, 'nuevo_producto.html',{'formulario':form,'accion':'Nuevo'})

def editar_producto(request, id):
    producto = Producto.objects.get(pk=id)
    if request.method == "POST":
        form = FormProducto(request.POST, instance=producto)
        if form.is_valid():
            producto = form.save()
            producto.save()
            return redirect('productos')
    else:
        form = FormProducto(instance=producto)
    return render(request, 'nuevo_producto.html', {'formulario': form,'accion':'Editar'})

def eliminar_producto(request):
    id = request.POST.get("codigo_ean")
    producto = Producto.objects.get(pk=id)
    producto.delete()
    return redirect('productos')