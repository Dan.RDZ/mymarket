from django.db import models

# Create your models here.
class Producto(models.Model):
    codigo_ean= models.IntegerField('Codigo ean', primary_key=True)
    nombre_producto = models.CharField('Nombre',max_length=100)
    descripcion = models.CharField('Descripción', max_length=300)
    precio = models.DecimalField('Precio Unitario',max_digits=5, decimal_places=2)
    cantidad = models.IntegerField('Cantidad')