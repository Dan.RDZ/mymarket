from django.urls import path
from productos.views import *

urlpatterns = [
    path('', lista_productos, name='productos'),
    path('nuevo', nuevo_producto, name='nuevo_producto'),
    path('editar/<int:id>', editar_producto, name='editar_producto'),
    path('eliminar', eliminar_producto, name='eliminar_producto'),
]
