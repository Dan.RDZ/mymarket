from django.test import TestCase
from productos.models import Producto
from productos.forms import FormProducto

class productosTest(TestCase):
    def test_verPaginaProductos(self):
        response = self.client.get('/productos/')
        self.assertEquals(response.status_code, 200)
    
    def test_verificarTemplateProductos(self):
        response = self.client.get('/productos/')
        self.assertTemplateUsed(response, 'productos.html')
    
    def test_verPaginaNuevoProducto(self):
        response = self.client.get('/productos/nuevo')
        self.assertEquals(response.status_code, 200)
    
    def test_verificarTemplateNuevoProducto(self):
        response = self.client.get('/productos/nuevo')
        self.assertTemplateUsed(response, 'nuevo_producto.html')
    
    def test_guardar_producto_form(self):
        producto = Producto.objects.create(
            codigo_ean = '02398712',
            nombre_producto = 'Ingrata',
            descripcion = 'No me digas que me quieres',
            precio = '250',
            cantidad = '5'
        )
        form = FormProducto(
            data={
                'codigo_ean': producto.codigo_ean,
                'nombre_producto': producto.nombre_producto,
                'descripcion': producto.descripcion,
                'precio': producto.precio,
                'cantidad': producto.cantidad
            }
        )
        if form.is_valid():
            new_producto = form.save()
            self.assertEqual(new_producto, Producto.objects.all()[0])

    def test_eliminar_producto(self):
        producto = Producto.objects.create(
            codigo_ean = '300',
            nombre_producto = 'Perra',
            descripcion = 'No funciona biena',
            precio = '250',
            cantidad = '5'
        )
        form = FormProducto(
            data={
                'codigo_ean': producto.codigo_ean,
                'nombre_producto': producto.nombre_producto,
                'descripcion': producto.descripcion,
                'precio': producto.precio,
                'cantidad': producto.cantidad
            }
        )
        if form.is_valid():
            new_producto = form.save()
            new_producto = form.delete()
            self.assertFalse(new_producto, Producto.objects.all(new_producto.id)[0])