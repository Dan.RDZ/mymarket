from django import forms
from .models import Producto

class FormProducto(forms.ModelForm):

    class Meta:
        model = Producto
        fields = ('codigo_ean','nombre_producto','descripcion','precio','cantidad')

        error_messages = {
            'codigo_ean' :{'required' : 'Debes ingresar un codigo ean'},
            'nombre_producto': {'required': 'Debes ingresar un nombre'},
            'descripcion': {'required': 'Debes ingresar una descripcion'},
            'precio': {'required': 'Debes ingresar un precio'},
            'cantidad': {'required': 'Debes ingresar una cantidad'}
        }