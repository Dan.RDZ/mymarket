Característica: Ventas
    Como vendedor de emarket
    Quiero realizar una venta
    Para poder ganar dinero

    Escenario: agregar producto a carrito
        Dado que estoy en la pagina principal
        Cuando preciono en productos 
        y presiono agregar al carrito  
        Entonces puedo ver las pagina exito.

    Escenario: ver lista de carritos
    	Dado que estoy en la pagina principal
    	cuando preciono carrito
    	Entonces puedo ver la pagina con la lista

    Escenario: Hacer venta 
    	Dado que estoy en la pagina de lista productos
    	Cuando preciono hacer venta
    	Entonces puedo ver la pagina de exito

    Escenario: Ver lista ventas
    	Dado que estoy en la pagina de principal
    	Cuando preciono lista ventas
    	Entonces puedo ver las ventas	 	    
    