Característica: Productos
    Como usuario de eMarket
    Quiero agregar un producto
    Para poder tener un inventario

    Escenario: Ver la lista de productos
        Dado que estoy en la pagina principal
        Cuando selecciono la seccion de productos
        Entonces puedo ver los productos existentes
    
    Escenario: Agregar producto
        Dado que tengo la informacion de un nuevo producto
        Cuando agrego producto nuevo en el inventario
        Entonces deberia ver que se agrego exitosamente en el inventario
    
    Escenario: Modificar producto
        Dado que tengo un producto ya registado 
        Cuando realizo modificar producto
        Entonces puedo ver que se actualizado su informacion
    
    Escenario: Eliminar producto
        Dado que ya no se provee un producto 
        Cuando realizo eliminar producto
        Entonces el producto se borra del sistema