Característica: CRUD Proveedores
    Como usuario de eMarket
    Quiero manejar mis contactos de proveedores
    Para poder contactarlos en caso de que se me acaben los productos

    Escenario: Ver Lista Proveedores
        Dado que tengo una lista de proveedores
        Cuando entro a la lista de proveedores
        Entonces me mostrará a todos los proveedores existentes

    Escenario: Agregar un proveedor
        Dado que estoy en la lista de proveedores
        Cuando doy clic en "Nuevo"
        Y introduzco "Colette" como Nombre
        Y introduzco "Coco" como Apellido
        Y introduzco "The Coca-Cola Company" como Compania
        Y introduzco "4925556565" como Numero Telefonico
        Y doy clic en el boton "Guardar"
        Entonces puedo ver a "Colette" como Proveedor en la Lista de Proveedores

    Escenario: Modificar un proveedor
        Dado que estoy en la lista de proveedores
        Cuando encuentro a "Colette" en la lista de proveedores
        Y doy clic en "Editar"
        Y introduzco "Cody" como Nombre
        Y introduzco "Brian" como Apellido
        Y introduzco "Pepsico" como Compania
        Y introduzco "4925557777" como Numero Telefonico
        Y doy clic en el boton "Guardar"
        Entonces puedo ver a "Cody" como Proveedor en la Lista de Proveedores

    Escenario: Eliminar un proveedor
        Dado que estoy en la lista de proveedores
        Cuando encuentro a "Cody" en la lista de proveedores
        Y doy clic en "Eliminar"
        Entonces puedo ver que "Cody" no pertenece a la Lista de Proveedores