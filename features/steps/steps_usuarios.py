from behave import given, when, then
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time

driver = webdriver.Firefox()
@given(u'que estoy en la página de inicio de sesión')
def step_impl(context):
    driver.get('http://localhost:8000/accounts/login')

@when(u'introduzco "{nombre_usuario}" como nombre de usuario')
def step_impl(context,nombre_usuario):
    driver.find_element_by_xpath('//*[@id="id_username"]').send_keys(nombre_usuario)

@when(u'introduzco "{contrasena}" como contraseña')
def step_impl(context,contrasena):
    driver.find_element_by_xpath('//*[@id="id_password"]').send_keys(contrasena)

@then(u'puedo ver el titulo de la pagina "{titulo}".')
def step_impl(context,titulo):
    time.sleep(1)
    driver.find_element_by_xpath('/html/body/form/button').click()
    time.sleep(2)
    titulo_res = context.drive.title
    assert titulo == titulo_res, "El titulo esperado "+titulo+" no es igual al resultado "+titulo_res

@then(u'puedo ver que me regresa a la pagina de login.')
def step_impl(context):
    driver.find_element_by_xpath('/html/body/form/button').click()
    time.sleep(2)
    titulo_res = context.drive.title
    assert  "login" == titulo_res, "El titulo esperado login no es igual al resultado "+titulo_res