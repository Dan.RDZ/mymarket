from behave import given, when, then
from django.conf import settings
from selenium import webdriver

@when(u'selecciono la seccion de productos')
def step_impl(context):
    pass

@then(u'puedo ver los productos existentes.')
def step_impl(context):
    pass

@then(u'puedo ver los productos existentes')
def step_impl(context):
    raise NotImplementedError(u'STEP: Then puedo ver los productos existentes')


@given(u'que tengo la informacion de un nuevo producto')
def step_impl(context):
    raise NotImplementedError(u'STEP: Given que tengo la informacion de un nuevo producto')


@when(u'agrego producto nuevo en el inventario')
def step_impl(context):
    raise NotImplementedError(u'STEP: When agrego producto nuevo en el inventario')


@then(u'deberia ver que se agrego exitosamente en el inventario')
def step_impl(context):
    raise NotImplementedError(u'STEP: Then deberia ver que se agrego exitosamente en el inventario')


@given(u'que tengo un producto ya registado')
def step_impl(context):
    raise NotImplementedError(u'STEP: Given que tengo un producto ya registado')


@when(u'realizo modificar producto')
def step_impl(context):
    raise NotImplementedError(u'STEP: When realizo modificar producto')


@then(u'puedo ver que se actualizado su informacion')
def step_impl(context):
    raise NotImplementedError(u'STEP: Then puedo ver que se actualizado su informacion')


@given(u'que ya no se provee un producto')
def step_impl(context):
    raise NotImplementedError(u'STEP: Given que ya no se provee un producto')


@when(u'realizo eliminar producto')
def step_impl(context):
    raise NotImplementedError(u'STEP: When realizo eliminar producto')


@then(u'el producto se borra del sistema')
def step_impl(context):
    raise NotImplementedError(u'STEP: Then el producto se borra del sistema')