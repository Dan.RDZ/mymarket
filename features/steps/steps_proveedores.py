from behave import given, when, then
from django.conf import settings
from selenium import webdriver
import time
from selenium.common.exceptions import NoSuchElementException


@given(u'que tengo una lista de proveedores')
def step_impl(context):
    pass


@when(u'entro a la lista de proveedores')
def step_impl(context):
    context.browser.get('http://localhost:8000/vendedor/proveedores')


@then(u'me mostrará a todos los proveedores existentes')
def step_impl(context):
    context.browser.quit()
    

@given(u'que estoy en la lista de proveedores')
def step_impl(context):
    context.browser.get('http://localhost:8000/vendedor/proveedores')


@when(u'doy clic en "{enlace}"')
def step_impl(context, enlace):
    time.sleep(3)
    context.browser.find_element_by_link_text(enlace).click()


@when(u'introduzco "{nombre}" como Nombre')
def step_impl(context, nombre):
    time.sleep(3)
    name = context.browser.find_element_by_id('id_nombre')
    time.sleep(1)
    name.clear()
    time.sleep(1)
    name.send_keys(nombre)


@when(u'introduzco "{apellido}" como Apellido')
def step_impl(context, apellido):
    time.sleep(3)
    ap = context.browser.find_element_by_id('id_primerapeido')
    time.sleep(1)
    ap.clear()
    time.sleep(1)
    ap.send_keys(apellido)


@when(u'introduzco "{compania}" como Compania')
def step_impl(context, compania):
    time.sleep(3)
    comp = context.browser.find_element_by_id('id_compania')
    time.sleep(1)
    comp.clear()
    time.sleep(1)
    comp.send_keys(compania)


@when(u'introduzco "{telefono}" como Numero Telefonico')
def step_impl(context, telefono):
    time.sleep(3)
    tel = context.browser.find_element_by_id('id_telefono')
    time.sleep(1)
    tel.clear()
    time.sleep(1)
    tel.send_keys(telefono)


@when(u'doy clic en el boton "{boton}"')
def step_impl(context, boton):
    time.sleep(3)
    context.browser.find_element_by_xpath("//button[text()='"+boton+"']").click()


@then(u'puedo ver a "{nombre}" como Proveedor en la Lista de Proveedores')
def step_impl(context, nombre):
    time.sleep(5)
    context.browser.find_element_by_xpath("//*[text()='"+nombre+"']")


@when(u'encuentro a "{usuario}" en la lista de proveedores')
def step_impl(context, usuario):
    time.sleep(3)
    context.browser.find_element_by_xpath("//*[text()='"+usuario+"']")

@then(u'puedo ver que "{usuario}" no pertenece a la Lista de Proveedores')
def step_impl(context, usuario):
    time.sleep(5)
    try:
        context.browser.find_element_by_xpath("//*[text()='"+usuario+"']")
    except NoSuchElementException as nsee:
        pass