from behave import given, when, then
from django.conf import settings

@given(u'que estoy en la pagina principal')
def step_impl(context):
    raise NotImplementedError(u'STEP: Given que estoy en la pagina principal')

@when(u'preciono en productos')
def step_impl(context):
    raise NotImplementedError(u'STEP: When preciono en productos')


@when(u'presiono agregar al carrito')
def step_impl(context):
    raise NotImplementedError(u'STEP: When presiono agregar al carrito')


@then(u'puedo ver las pagina exito.')
def step_impl(context):
    raise NotImplementedError(u'STEP: Then puedo ver las pagina exito.')


@when(u'preciono carrito')
def step_impl(context):
    raise NotImplementedError(u'STEP: When preciono carrito')


@then(u'puedo ver la pagina con la lista')
def step_impl(context):
    raise NotImplementedError(u'STEP: Then puedo ver la pagina con la lista')


@given(u'que estoy en la pagina de lista productos')
def step_impl(context):
    raise NotImplementedError(u'STEP: Given que estoy en la pagina de lista productos')


@when(u'preciono hacer venta')
def step_impl(context):
    raise NotImplementedError(u'STEP: When preciono hacer venta')


@then(u'puedo ver la pagina de exito')
def step_impl(context):
    raise NotImplementedError(u'STEP: Then puedo ver la pagina de exito')


@given(u'que estoy en la pagina de principal')
def step_impl(context):
    raise NotImplementedError(u'STEP: Given que estoy en la pagina de principal')


@when(u'preciono lista ventas')
def step_impl(context):
    raise NotImplementedError(u'STEP: When preciono lista ventas')


@then(u'puedo ver las ventas')
def step_impl(context):
    raise NotImplementedError(u'STEP: Then puedo ver las ventas')