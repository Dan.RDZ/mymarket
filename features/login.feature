Característica: Iniciar sesión
    Como usuario de eMarket
    Quiero iniciar sesión
    Para poder usar el sistema de eMarket

    Escenario: Iniciar sesión como vendedor
        Dado que estoy en la página de inicio de sesión
        Cuando introduzco "usuario123" como nombre de usuario
        Y introduzco "contra123" como contraseña
        Entonces puedo ver el titulo de la pagina "productos".
    
    Escenario: Iniciar sesión como otro  vendedor
        Dado que estoy en la página de inicio de sesión
        Cuando introduzco "elusuariochingon" como nombre de usuario
        Y introduzco "123abc3312" como contraseña
        Entonces puedo ver el titulo de la pagina "productos".
    
    Escenario: Iniciar sesión como usuario no valido
        Dado que estoy en la página de inicio de sesión
        Cuando introduzco " " como nombre de usuario
        Y introduzco " " como contraseña
        Entonces puedo ver que me regresa a la pagina de login.