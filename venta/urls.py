from django.urls import path
from venta.views import *

urlpatterns = [
     path('carritos', ver_carrito, name='ver_carrito'),
     path('venta_realizada', realizar_venta, name='realizar_venta'),
     path('agregar_carrito/<int:pk>', agregar_carrito, name='exito'),

    
]
