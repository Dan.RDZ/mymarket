from django.db import models
from productos.models import Producto
# Create your models here.

class Venta(models.Model):
    fecha_venta = models.DateTimeField()
    total_venta = models.DecimalField('Total de venta',max_digits=7, decimal_places=2)


class Carrito(models.Model):
    id_lista = models.ForeignKey(Venta, on_delete=models.CASCADE)
    producto = models.ForeignKey(Producto, on_delete=models.CASCADE)
    cantidad = models.IntegerField()
