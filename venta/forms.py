from django import forms
from .models import Venta

class FormVenta(forms.ModelForm):

    class Meta:
        model = Venta
        fields = ('fecha_venta', 'total_venta')