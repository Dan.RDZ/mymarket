from django.test import TestCase
from venta.models import Venta
from venta.forms import FormVenta
from datetime import datetime, timedelta , date
from productos.models import Producto
# Create your tests here.

class VentasTest(TestCase):

    def testPaginaVerCarrito(self):
    	venta = Venta.objects.create(
    		fecha_venta = datetime.now(),
    		total_venta = '0'
    		)
    	response = self.client.get('/venta/carritos')
    	self.assertEqual(response.status_code, 200)

    def testPaginaVerTemplate(self):
    	venta = Venta.objects.create(
    		fecha_venta = datetime.now(),
    		total_venta = '0'
    		)
    	response = self.client.get('/venta/carritos')
    	self.assertTemplateUsed(response, 'carritos.html')

    def test_guardar_venta_form(self):
        venta = Venta.objects.create(
            fecha_venta = datetime.now(),
            total_venta = '250'
        )
        form = FormVenta(
        	data={
        		'fecha_venta': venta.fecha_venta,
        		'total_venta': venta.total_venta

        	}
        ) 
        if form.is_valid():
            new_venta = form.save()
            self.assertEqual(new_venta, Venta.objects.latest('id'))  

    def testRealizarVenta(self):
    	producto1 = Producto.objects.create(
    		codigo_ean = '02398712',
    		nombre_producto = 'Ingrata',
            descripcion = 'No me digas que me quieres',
            precio = '250',
            cantidad = '5'
            )
    	producto2 = Producto.objects.create(
    		codigo_ean = '02398342',
    		nombre_producto = 'Ingrata',
    		descripcion = 'No me digas que me quieres',
    		precio = '250',
    		cantidad = '5'
    		)
    	venta = Venta.objects.create(
    		fecha_venta = datetime.now(),
    		total_venta = '0'
    		)        
    	response = self.client.get("/venta/venta_realizada")

    	self.assertEqual(response.status_code, 200)

    def testAgregarCarrito(self):
        fecha= datetime.now()
        producto1 = Producto.objects.create(
            codigo_ean = '02398712',
            nombre_producto = 'Ingrata',
            descripcion = 'No me digas que me quieres',
            precio = '250',
            cantidad = '5'
            )
        venta = Venta.objects.create(
            fecha_venta = fecha,
            total_venta = '0'
            )     	
        producto1.save()
        response=self.client.get("/venta/agregar_carrito/02398712")
        self.assertTemplateUsed(response, 'exito.html')

    def testAgregarCarritoExistente(self):
        fecha= datetime.now()
        producto1 = Producto.objects.create(
            codigo_ean = '02398712',
            nombre_producto = 'Ingrata',
            descripcion = 'No me digas que me quieres',
            precio = '250',
            cantidad = '5'
            )
        venta = Venta.objects.create(
            fecha_venta = fecha,
            total_venta = '24'
            ) 

        venta.save()          
        producto1.save()
        response=self.client.get("/venta/agregar_carrito/02398712")
        self.assertTemplateUsed(response, 'exito.html')                                          	            