from django.shortcuts import render
from .models import Carrito
from .models import Venta
from productos.models import Producto
from datetime import datetime, timedelta , date
# Create your views here.
def ver_carrito(request):
	venta = Venta.objects.latest('id')
	if venta.total_venta == 0:
		carritos = Carrito.objects.filter(id_lista=venta)
		return render(request,'carritos.html', {'carritos': carritos})

def realizar_venta(request):
	venta = Venta.objects.latest('id')
	carrito = Carrito.objects.filter(id_lista=venta)
	cuenta = 0
	for nom in carrito[0:len(carrito)]:
		producto = Producto.objects.get(nom.producto)
		cuenta += producto.precio
		venta.total_venta = cuenta
		producto.cantida = producto.cantida - nom.cantidad
		producto.save()
		venta.save()
	return render(request,'venta_realizada.html')	

def agregar_carrito(request, pk):
	venta = Venta.objects.latest('id')
	producto = Producto.objects.get(codigo_ean = pk)
	if venta.total_venta == 0:
		carrito = Carrito.objects.create(id_lista=venta, producto = producto, cantidad = 1)
		carrito.save()
		return render(request,'exito.html')
	else:
		nueva_venta = Venta.objects.create(fecha_venta = datetime.now(), total_venta = '0')
		nueva_venta.save()
		carrito = Carrito.objects.create(id_lista=nueva_venta, producto = producto, cantidad = 1)
		carrito.save()
		return render(request,'exito.html')					


	
